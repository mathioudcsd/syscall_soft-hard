all: run run2

run: run.o
	gcc run.o -o run

run2: run2.o
	gcc run2.o -o run2

run.o: demo.c
	gcc -c demo.c -o run.o

run2.o:
	gcc -c demo2.c -o run2.o

clean:
	rm run run2 run.o run2.o