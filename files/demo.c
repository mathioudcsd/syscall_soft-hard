#include <stdlib.h>
#include <errno.h>
#include <sys/mman.h>
#define SET 341
#define GET 342

struct d_params{
	int hard_deadline;
	int soft_deadline;
	int expected_computation;
};

static int *gvar;

void childProcess(){
	*gvar = getpid();
	sleep(4);
}

void parentProcess(){
	struct d_params *kouvas=malloc(sizeof(struct d_params));
	sleep(2);
	syscall(SET, *gvar, 100, 200, 50);
	syscall(SET, -1, 66, 666, 69 );
	//syscall(SET, 900, 8);
	syscall(GET, -1, kouvas);
	printf("DAD:My pramata: SOFT:%d HARD: %d EXPECTED: %d \n", kouvas->soft_deadline, kouvas->hard_deadline, kouvas->expected_computation);
	syscall(GET, *gvar, kouvas);
	printf("DAD:kid's SOFT:%d HARD: %d EXPECTED: %d \n", kouvas->soft_deadline, kouvas->hard_deadline, kouvas->expected_computation);
	munmap(gvar, sizeof *gvar);
}

int main(){
	gvar = mmap(NULL, sizeof *gvar, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	if(fork()==0) {/* child */
		childProcess();
	}
	else
		parentProcess();
	return 0;
}
