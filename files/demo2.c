#include <stdlib.h>
#include <errno.h>
#include <sys/mman.h>
#define SET 341
#define GET 342

struct d_params{
	int hard_deadline;
	int soft_deadline;
	int expected_computation;
};

static int *gvar;

void childProcess(){
	sleep(1);
	struct d_params *kouti =malloc(sizeof(struct d_params));
	printf("KID:my dad'sSOFT:%d HARD: %d EXPECTED: %d \n", kouti->soft_deadline, kouti->hard_deadline, kouti->expected_computation);
	kouti->soft_deadline=-99;
	kouti->hard_deadline=2;
	kouti->expected_computation=5;
	syscall(GET, *gvar, kouti);
	printf("KID:my SOFT:%d HARD: %d EXPECTED: %d \n", kouti->soft_deadline, kouti->hard_deadline, kouti->expected_computation);
}

void parentProcess(){
	syscall(SET, -1, 334, 333, 93);
	*gvar = getpid();
	sleep(2);
	munmap(gvar, sizeof *gvar);
}

int main(){
	gvar = mmap(NULL, sizeof *gvar, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	if(fork()==0) {/* child */
		childProcess();
	}
	else
		parentProcess();
	return 0;
}
