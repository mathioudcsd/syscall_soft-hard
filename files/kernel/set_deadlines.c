#include <linux/kernel.h>
#include <linux/syscalls.h>
#include <asm/uaccess.h>
#include <linux/d_params.h>
#include <asm-generic/errno-base.h>
#include <asm/current.h>
#include <linux/slab.h>

struct task_struct *search_process(int target_pid){
	//int found_flag = 0;
	struct task_struct *p;
	struct task_struct *target=NULL;
	for_each_process(p){
		if(p->pid == target_pid){
			target=p;
			return target;
		}
		else 
			continue;
	}
	return NULL;
}

asmlinkage long sys_set_deadlines(int pid, int soft, int hard, int expected){
	printk("mathioud_AM2720 - sys_SET_deadlines\n");
	struct list_head *p;
	struct task_struct *task;
	struct task_struct *victim;
	struct task_struct *buff;
	// ASKHSH 4
	struct ddln_struct *ddln_buff;

	struct task_struct *curr_task = get_current();
	int curr_pid = curr_task->pid;
	extern struct ddln_struct *ddln_HEAD;
	if(pid<-1){
		printk("Invalid pid(set_deadlines)\n");
		return EINVAL;

	}

	if(pid==curr_pid || pid==-1){ //eautos
		if(hard>soft && hard>0 && soft>0){
			curr_task->soft_deadline = soft;
			curr_task->hard_deadline = hard;
			curr_task->expected_computation = expected;

			// ASKHSH 4
			ddln_buff = kmalloc(sizeof(struct ddln_struct), GFP_KERNEL);
			ddln_buff->pid = curr_pid;
			ddln_buff->tp = curr_task;
			ddln_buff->next = ddln_HEAD;
			ddln_HEAD = ddln_buff;

			printk("PARENT: soft deadline = %d\n", curr_task->soft_deadline);
			printk("PARENT: hard deadline = %d\n", curr_task->hard_deadline);
			printk("PARENT: expected computation = %d\n", curr_task->expected_computation);
		}
		else{
			printk("LATHOS TIMES STA HARD 'H SOFT KSANADES TO\n");
			return EINVAL;
		}
	} 
	else{
		//printk("MPHKA big else \n");
		victim = search_process(pid);
		// buff = victim;
		if(victim==NULL){
		 	//printk("victim = NULL\n");
			return EINVAL;
		}
		else{
			if(victim->parent->pid == curr_pid){
				//printk(" VRHKA TO PAIDI\n");
				if(hard>soft && hard>0 && soft>0){
					victim->soft_deadline = soft;
					victim->hard_deadline = hard;
					victim->expected_computation = expected;

					ddln_buff = kmalloc(sizeof(struct ddln_struct), GFP_KERNEL);
					ddln_buff->pid = curr_pid;
					ddln_buff->tp = victim;
					ddln_buff->next = ddln_HEAD;
					ddln_HEAD = ddln_buff;

					printk("GIOKAS: soft deadline = %d\n", victim->soft_deadline);
					printk("GIOKAS: hard deadline = %d\n", victim->hard_deadline);
					printk("GIOKAS: expected computation = %d\n", victim->expected_computation);
					return 0;	
				}
				else{
					printk("FILARAKI LATHOS TIMES STA HARD 'H SOFT KSANADES TO\n");
					return EINVAL;
				}
			}
			else{
				printk("No child with pid=%d found!\n", pid);
				//total_demand=0;
				return EINVAL;
			}
		}
	}		

}
