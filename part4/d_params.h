struct d_params{
	int hard_deadline;
	int soft_deadline;
	int expected_computation;
};

// ASKHSH 4

struct ddln_struct{
	int pid;
	struct task_struct *tp;
	struct ddln_struct* next;
};